base:
  'G@os:Ubuntu or G@os:Debian':
    - match: compound
    - base

  'bgp-*':
    - pip
    - network
    - exabgp
    - nginx
