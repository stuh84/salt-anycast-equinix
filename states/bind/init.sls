bind9:
  pkg.installed

dnsutils:
  pkg.installed

/etc/bind/named.conf.options:
  file.managed:
    - source: salt://bind/files/named.conf.options.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

/etc/resolv.conf:
  file.managed:
    - source: salt://bind/files/resolv.conf.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

bind9.service:
  service.running:
    - enable: True
    - reload: True
    - onchanges:
      - file: /etc/bind/named.conf.options
