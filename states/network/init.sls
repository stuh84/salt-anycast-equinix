{% for interface in pillar['network']['interfaces'] %}
{{ interface['name'] }}:
  network.managed:
    - enabled: True
    {%- if interface['type'] is defined %}
    - type: {{ interface['type'] }}
    {%- endif %}
    {%- if interface['proto'] is defined %}
    - proto: {{ interface['proto'] }}
    {%- endif %}
    - ipaddr: {{ interface['ipv4']['address'] }}
    - netmask: {{ interface['ipv4']['mask'] }}
    {%- if interface['ipv4']['gateway'] is defined %}
    - gateway: {{ interface['ipv4']['gateway'] }}
    {%- endif %}
    {%- if interface['ipv4']['nameservers'] is defined %}
    - dns:
      {% for ns in interface['ipv4']['nameservers'] %}
      - {{ ns }}
      {% endfor %}
    {%- endif %}
{%- if interface['routes'] is defined %}
routes_{{ interface['name'] }}:
  network.routes:
    - name: {{ interface['name'] }}
    - routes:
       {%- for route in interface['routes'] %}
       - name: {{ route['name'] }}
         ipaddr: {{ route['prefix'] }}
         netmask: {{ route['mask'] }}
         gateway: {{ route['gateway'] }}
       {%- endfor %}
{%- endif %}
{% endfor %} 
