network:
  interfaces:
{% for ip in grains['anycast']['ipv4'] %}
    - name: "lo:{{ loop.index }}"
      type: eth
      proto: static
      ipv4:
        address: {{ ip['address'] }}
        mask: {{ ip['mask'] }}
{% endfor %}
